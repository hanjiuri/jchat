package cn.jiuri.jchat;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import cn.jiuri.jchat.api.API;
import cn.jiuri.jchat.common.CodeEnum;
import cn.jiuri.jchat.common.Core;
import cn.jiuri.jchat.model.User;
import cn.jiuri.jchat.msg.HandleMsgImpl;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @program: Jchat
 * @author: Mr.Han
 * @create: 2019-01-25 15:59
 **/
public class Test {
    private static Core core = Core.getInstance();
    private static Log log = LogFactory.get();
    //微信登陆二维码路径(mac)
    private final static String path = "/Users/hanjiuri/IdeaProjects/Jchat/qr/qrcode.jpg";
    //测试
    public static void main(String[] args) {
        //设置微信登陆二维码输出路径
        core.setQrCodePath(path);
        //登陆
        API.login();
        //欢迎登陆
        log.info("欢迎你,昵称:{},用户id:{}",core.getNickName(),core.getUserName());
        //获取通讯录好友
        Map<String,User> userMap = core.getUserMap();
        log.info("好友列表:{}",JSONUtil.toJsonStr(userMap));
        //消息处理(实现接口自定义实现)
        API.handleMsg(new HandleMsgImpl());
        //发送消息给微信文件助手
        API.sendMsg("hello jchat", "filehelper", CodeEnum.TEXT.getCode());
    }
}
