package cn.jiuri.jchat.common;

import cn.hutool.json.JSONObject;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import cn.jiuri.jchat.model.Msg;
import cn.jiuri.jchat.model.User;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @program: Jchat
 * @author: Mr.Han
 * @create: 2019-01-24 17:11
 **/
public class Core {
    private static Core instance;

    public static Core getInstance() {
        if (instance == null) {
            synchronized (Core.class) {
                instance = new Core();
            }
        }
        return instance;
    }

    //二维码路径
    private String qrCodePath;
    //是否登陆
    private boolean isLogin = false;
    //通讯录
    private ConcurrentHashMap<String, User> userMap;
    //消息队列
    private Queue<Msg> msgQueue = new LinkedBlockingQueue<Msg>();
    //参数
    private JSONObject paramMap;
    //用户名
    private String userName;
    //用户昵称
    private String nickName;
    //登陆cookie
    private String cookie;
    private static SSLSocketFactory ssf;

    static {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        TrustManager[] tm = {new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};
        //初始化
        try {
            sslContext.init(null, tm, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        //获取SSLSocketFactory对象
        ssf = sslContext.getSocketFactory();
    }

    public static SSLSocketFactory getSsf() {
        return ssf;
    }

    public static void setSsf(SSLSocketFactory ssf) {
        Core.ssf = ssf;
    }

    public String getQrCodePath() {
        return qrCodePath;
    }

    public void setQrCodePath(String qrCodePath) {
        this.qrCodePath = qrCodePath;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    public ConcurrentHashMap<String, User> getUserMap() {
        return userMap;
    }

    public void setUserMap(ConcurrentHashMap<String, User> userMap) {
        this.userMap = userMap;
    }

    public Queue<Msg> getMsgQueue() {
        return msgQueue;
    }

    public void setMsgQueue(Queue<Msg> msgQueue) {
        this.msgQueue = msgQueue;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public JSONObject getParamMap() {
        return paramMap;
    }

    public void setParamMap(JSONObject paramMap) {
        this.paramMap = paramMap;
    }

    public static void setInstance(Core instance) {
        Core.instance = instance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
