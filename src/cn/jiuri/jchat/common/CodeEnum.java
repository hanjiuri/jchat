package cn.jiuri.jchat.common;

/**
 * @program: Jchat
 * @author: Mr.Han
 * @create: 2019-02-13 13:35
 **/
public enum CodeEnum {
    TEXT("text",1);
    private String type;
    private Integer code;

    CodeEnum(String type, Integer code) {
        this.type = type;
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
