package cn.jiuri.jchat.common;

/**
 * @program: Jchat
 * @author: Mr.Han
 * @create: 2019-01-23 14:22
 **/
public class Constant {
    public static final String APP_ID = "wx782c26e4c19acffb";
    public static final String GET_UUID_API = "https://login.wx.qq.com/jslogin";
    public static final String SHOW_QR_CODE_API = "https://login.wx.qq.com/qrcode/%s";
    public static final String WAIT_LOGIN_API = "https://login.wx.qq.com/cgi-bin/mmwebwx-bin/login";
    public static final String WX_INIT_API = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxinit?pass_ticket=%s";
    public static final String WX_STATUS_NOTIFY = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxstatusnotify?lang=%s&pass_ticket=%s";
    public static final String WX_CONTACT = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetcontact?lang=zh_CN&seq=0&pass_ticket=%s&skey=%s";
    public static final String WX_CHECK_MSG = "https://webpush.wx.qq.com/cgi-bin/mmwebwx-bin/synccheck?r=%s&skey=%s&sid=%s&uin=%s&deviceid=%s&synckey=%s&_=%s";
    public static final String WX_GET_MSG = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsync?sid=%s&skey=%s&pass_ticket=%s";
    public static final String WX_SEND_MSG = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsg?pass_ticket=%s";
    public static final int TEXT = 0;
}
