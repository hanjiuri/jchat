package cn.jiuri.jchat.model;

/**
 * @program: Jchat
 * @author: Mr.Han
 * @create: 2019-01-25 16:21
 **/
public class Msg {
    private Integer Type;
    private String Content;
    private String FromUserName;
    private String ToUserName;
    private Long LocalID;

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String fromUserName) {
        FromUserName = fromUserName;
    }

    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String toUserName) {
        ToUserName = toUserName;
    }

    public Long getLocalID() {
        return LocalID;
    }

    public void setLocalID(Long localID) {
        LocalID = localID;
    }
}
