package cn.jiuri.jchat.msg;

/**
 * @program: Jchat
 * @author: Mr.Han
 * @create: 2019-02-13 10:46
 **/

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import cn.jiuri.jchat.api.API;
import cn.jiuri.jchat.common.CodeEnum;
import cn.jiuri.jchat.model.Msg;

/**
 * 消息处理(自定义)
 */
public class HandleMsgImpl implements HandleMsgInterface {
    private static Log log = LogFactory.get();
    @Override
    public void handleMsg(Msg msg) {
        log.info("消息内容:{},消息发送人:{},消息接收人:{}",msg.getContent(),msg.getFromUserName(),msg.getToUserName());
        //价值1亿的AI核心代码
        String msgStr = "";
        if(msg.getContent().contains("吗")){
            msgStr = msgStr.replaceAll("吗", "");
        }
        if(msg.getContent().contains("?")){
            msgStr = msgStr.replaceAll("\\?","");
        }
        if(msg.getContent().contains("？")){
            msgStr = msgStr.replaceAll("？","");
        }
        //回复给发送人
        API.sendMsg(msgStr,msg.getFromUserName(), CodeEnum.TEXT.getCode());
    }
}
