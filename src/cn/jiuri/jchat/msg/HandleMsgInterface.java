package cn.jiuri.jchat.msg;

import cn.jiuri.jchat.model.Msg;

/**
 * @program: Jchat
 * @author: Mr.Han
 * @create: 2019-02-13 10:45
 **/
public interface HandleMsgInterface {
    void handleMsg(Msg msg);
}
